package org.ecos.logic.appbarexercise;

import static org.ecos.logic.appbarexercise.MainActivity.A_GIVEN_KEY;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity2 extends AppCompatActivity {
    public static final String SECOND_GIVEN_KEY = "LOLAILO";
    private Button action;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = this.getIntent();
        String lolailo = intent.getStringExtra(A_GIVEN_KEY);
        lolailo = lolailo==null?"":lolailo;
        Log.e(MainActivity2.this.getClass().getCanonicalName(),lolailo);

        this.action = this.findViewById(R.id.button);
        this.action.setOnClickListener(this.onActionClick);
    }

    private View.OnClickListener onActionClick = v -> {
        Intent intent = new Intent();
        intent.putExtra(SECOND_GIVEN_KEY, "Hey you!");
        this.setResult(RESULT_OK,intent);
        this.finish();
    };
}