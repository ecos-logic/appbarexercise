package org.ecos.logic.appbarexercise;

import static org.ecos.logic.appbarexercise.MainActivity2.SECOND_GIVEN_KEY;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

public class MainActivity extends AppCompatActivity {

    public static final String A_GIVEN_KEY = "LERELE";
    private final ActivityResultContracts.StartActivityForResult contract = new ActivityResultContracts.StartActivityForResult();
    private ActivityResultLauncher<Intent> launcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        this.launcher = this.registerForActivityResult(contract, this::onActivityResult);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem item = menu.findItem(R.id.app_bar_switch);

        View actionView = item.getActionView();
        if(actionView != null) {
            SwitchCompat switchItem = actionView.findViewById(R.id.menuswitch);
            switchItem.setOnCheckedChangeListener(this::onCreateOptionsMenu);

            Button action = actionView.findViewById(R.id.menuboton);
            action.setOnClickListener(this::onAction);
        }
        return true;
    }

    private void onAction(View view) {
        Toast.makeText(MainActivity.this,"button pressed", Toast.LENGTH_SHORT).show();
    }

    private void onCreateOptionsMenu(CompoundButton compoundButton, boolean b) {
        Toast.makeText(MainActivity.this, "Estado:"+b, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            Toast.makeText(this, "GET BACK!", Toast.LENGTH_SHORT).show();
        }
        if (item.getItemId() == R.id.edit_menu) {

            Intent intent = new Intent(this, MainActivity2.class);
            intent.putExtra(A_GIVEN_KEY, "tikotiko");
            this.launcher.launch(intent);
            return true;
        }
        if (item.getItemId() == R.id.remove_menu) {
            Toast.makeText(this, "Borrar", Toast.LENGTH_SHORT).show();
            return true;
        }
        return true;
    }

    private void onActivityResult(ActivityResult result) {
        if (result.getResultCode() == RESULT_OK) {
            if (result.getData() != null) {
                String lerele = result.getData().getStringExtra(SECOND_GIVEN_KEY);
                lerele = lerele == null ? "" : lerele;
                Toast.makeText(this.getApplicationContext(), lerele, Toast.LENGTH_LONG).show();
            }
        }
    }
}